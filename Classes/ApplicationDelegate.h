//
//  ApplicationDelegate.h
//  PushMeBaby
//
//  Created by Stefan Hafeneger on 07.04.09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "ioSock.h"

@interface ApplicationDelegate : NSObject {
	NSString *_deviceToken, *_payload, *_certificate;
	otSocket socket;
	SSLContextRef context;
	SecKeychainRef keychain;
	SecCertificateRef certificate;
	SecIdentityRef identity;
    
    NSTextField *tokenText;
    NSButton *tokenCheckBox;
    NSTextField *messageTextField;
    NSButton *messageCheckBox;
    NSTextField *badgetTextField;
    NSButton *badgeCheckBox;
    NSButton *badgeClick;
    NSTextField *soundTextField;
    NSButton *soundCheckBox;
    NSTextField *extraTextField;
    NSButton *extraCheckBox;
    NSButton *extraClick;
    NSButton *contentAvailableCheckBox;
    NSButton *contentAvailableClick;
    NSTextField *finalPayloadTextField;
}
#pragma mark IBAction
- (IBAction)push:(id)sender;

//Plus version code

@property (strong, nonatomic) IBOutlet NSMutableDictionary *payloadPush;

@property (assign) IBOutlet NSTextField *tokenText;
@property (assign) IBOutlet NSButton *tokenCheckBox;

@property (assign) IBOutlet NSTextField *messageTextField;
@property (assign) IBOutlet NSButton *messageCheckBox;

@property (assign) IBOutlet NSTextField *badgetTextField;
@property (assign) IBOutlet NSButton *badgeCheckBox;

@property (assign) IBOutlet NSTextField *soundTextField;
@property (assign) IBOutlet NSButton *soundCheckBox;

@property (assign) IBOutlet NSTextField *extraTextField;
@property (assign) IBOutlet NSButton *extraCheckBox;


@property (assign) IBOutlet NSButton *contentAvailableCheckBox;


@property (assign) IBOutlet NSTextField *finalPayloadTextField;

- (IBAction)openBucketSource:(id)sender;

@end
